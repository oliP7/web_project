var mongoose = require('mongoose');
var book = mongoose.model('book');

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.listBooks = function(req, res) {

    book.find()
        .select('title author ISBN')
        .exec(function (err, results) {
            returnJson(res, 200, results);

        });
}

module.exports.deleteBook = function(req, res) {

    var bookTitle = req.body.title;
    var bookISBN = req.body.ISBN;
    book.remove({title: bookTitle},{ISBN:bookISBN})
        .exec(function(err) {
            var deleted = (err ? false : true);
            returnJson(res, 200, {"deleted": deleted});
        });
}

module.exports.saveBook = function(req, res) {
    var bookTitle = req.params.title;
    var bookAuthor = req.params.author;
    var bookISBN = req.params.ISBN;
    var bookLink = req.params.link;
    var bookSubject = req.params.subject;
    var bookNumOfCopies = req.params.numberOfCopies;
    var bookReleaseDate = req.params.releaseDate;
    var bookImg = req.params.image;
    
    var pattern = /(\d{2})\-(\d{4})/;
    var bookDate = new Date(bookReleaseDate.replace(pattern,'$2-$1'));
    
    //Save file to DB
    var bookDB = new book({
        _id: new mongoose.Types.ObjectId(),
        title: bookTitle,
        author: bookAuthor,
        ISBN: bookISBN,
        link: bookLink,
        subject: bookSubject,
        numberOfCopies: bookNumOfCopies,
        releaseDate: bookDate,
        image: bookImg
    });
    
    bookDB.save(function(err) {
        var saved = (err ? false : true);
        returnJson(res, 200, {"saved": saved});
    });
}
var mongoose = require('mongoose');
var user = mongoose.model('user');
var book = mongoose.model('book');
var quote = mongoose.model('quote');

var usersList = [
        {username:"bozen@jovanovski.com",password:"pass123", name:"Bozen", surname:"Jovanovski"},
        {username:"amra@omanovic.com",password:"pass123", name:"Amra", surname:"Omanovic"},
        {username:"oli@perunkovska.com",password:"pass123", name:"Olivera", surname:"Perunkovska"}
        ];

var booksList =[
        {title:"Anna Karenina",author:"Lav Tolstoj", isbn:"25352", link:"", subject:"1",numberOfCopies:10, releaseDate: Date.now(),image:"/images/anaKarenjina.jpg"},
        {title:"One Hundred Years of Solitude",author:"Gabriel Garcia Marquez", isbn:"25352", link:"", subject:"1",numberOfCopies:10, releaseDate: Date.now(),image:"/images/stoGodinaSamoce.jpg"},
        {title:"Death and the Dervish",author:"Mesa Selimovic", isbn:"25352", link:"", subject:"1",numberOfCopies:10, releaseDate: Date.now(),image:"/images/dervisISmrt.jpg"},
        {title:"Antigona",author:"Sofocles", isbn:"25352", link:"", subject:"1",numberOfCopies:10, releaseDate: Date.now(),image:"/images/antigona.jpg"},
        {title:"Shadows of the Pomegranate Tree",author:"Tariq Ali", isbn:"25352", link:"", subject:"1",numberOfCopies:10, releaseDate: Date.now(),image:"/images/sjene.jpg"}

    ];

var quotesList =[
    {author:"Thomas Jefferson",quoteText:"I cannot live without books."},
];

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.deleteAllData = function (req, res) {

    user.deleteMany({}).exec(function (err) {
        if(err && !err){
            console.log("Problem deleting users!");
            returnJson(res, 200, {"deleted": false});
            return;
        }
    });
    book.deleteMany({}).exec(function (err) {
        if(err && !err){
            console.log("Problem deleting books!");
            returnJson(res, 200, {"deleted": false});
            return;
        }
    });
    quote.deleteMany({}).exec(function (err) {
        if(err && !err){
            console.log("Problem deleting quotes!");
            returnJson(res, 200, {"deleted": false});
            return;
        }
    });
    returnJson(res, 200, {"deleted": true});
}

module.exports.initDbData = function (req, res) {
    var listOfUsersForDb=[];
    usersList.forEach(function (value) {
        console.log(value.username+ " "+value.password);
        var userDB = new user({
            _id: new mongoose.Types.ObjectId(),
            username: value.username,
            password: value.password,
            name: value.name,
            surname: value.surname,
            orders: [],
            rents: [],
            returns: []
        });
        listOfUsersForDb.push(userDB);
    });
    user.insertMany(listOfUsersForDb, function (err) {
        if(err && !err) {
            console.log("Problem with inserting users: " + err);
            returnJson(res, 200, {"inserted": false});
            return;
        }
    });

    var listOfBooksForDB = [];
    booksList.forEach(function (value) {
        console.log(value.title+ " "+value.author);
        var bookDB = new book({
            _id: new mongoose.Types.ObjectId(),
            title: value.title,
            author: value.author,
            isbn: value.isbn,
            link: value.link,
            subject: value.subject,
            numberOfCopies: value.numberOfCopies,
            releaseDate: value.releaseDate,
            image: value.image
        });
        listOfBooksForDB.push(bookDB);
    });
    book.insertMany(listOfBooksForDB, function (err) {
        if(err && !err) {
            console.log("Problem with inserting books: " + err);
            returnJson(res, 200, {"inserted": false});
            return;
        }
    });

    var listOfQuotesForDB = [];
    quotesList.forEach(function (value) {
        console.log(value.author+ " "+value.quoteText);
        var quoteDB = new quote({
            _id: new mongoose.Types.ObjectId(),
            author: value.author,
            quoteText: value.quoteText
        });
        listOfQuotesForDB.push(quoteDB);
    });
    quote.insertMany(listOfQuotesForDB, function (err) {
        if(err && !err) {
            console.log("Problem with inserting quotes: " + err);
            returnJson(res, 200, {"inserted": false});
            return;
        }
    });


    returnJson(res, 200, {"inserted": true});
}
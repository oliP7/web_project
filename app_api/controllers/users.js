var mongoose = require('mongoose');
var user = mongoose.model('user');

var returnJson = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.userExists = function(req, res) {
    user.findOne({"username": req.params.username, "password": req.params.password})
        .exec(function(err, user) {
            var exists = (user ? true : false);
            returnJson(res, 200, {"exists": exists});
        });
};

module.exports.findUser = function(req, res) {
    user.findOne({"username": req.params.username})
        .exec(function(err, user) {
            var username = user.username;
            console.log("from db: "+user.username);
            var password = user.password;
            console.log("from db: "+user.password);
            var name = user.name;
            console.log("from db: "+user.name);
            var surname = user.surname;
            console.log("from db: "+user.surname);
            var exists = (user ? true : false);
            returnJson(res, 200, {"exists": exists, "name":name, "surname":surname, "username":username, "password":password});
        });
};

module.exports.listUsers = function(req, res) {

    user.find()
        .select('username password name surname')
        .exec(function (err, results) {
            returnJson(res, 200, results);

        });
}

module.exports.deleteUser = function(req, res) {

    var username = req.body.username;
    user.remove({username: username})
        .exec(function(err) {
            var deleted = (err ? false : true);
            returnJson(res, 200, {"deleted": deleted});
        });
}

module.exports.saveUser = function(req, res) {

    var usernameParam = req.params.username;
    var passwordParam = req.params.password;
    var nameParam = req.params.name;
    var surnameParam = req.params.surname;
    //Save file to DB
    var userDB = new user({
        _id: new mongoose.Types.ObjectId(),
        username: usernameParam,
        password: passwordParam,
        name: nameParam,
        surname: surnameParam
    });

    userDB.save(function(err) {
        var saved = (err ? false : true);
        returnJson(res, 200, {"saved": saved});
    });
}
var mongoose = require('mongoose');

var quotesSchema = new mongoose.Schema({
    author: {type: String, required: true},
    quoteText: {type: String, required: true},
});

mongoose.model('quote', quotesSchema, 'quotes');
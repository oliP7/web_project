var mongoose = require('mongoose');

var ordersSchema = new mongoose.Schema({
    bookName: {type: String}
});

var rentsSchema = new mongoose.Schema({
    bookName: {type: String},
    dueDate: {type: Date, "default": Date.now}
});

var returnsSchema = new mongoose.Schema({
    bookName: {type: String},
    returnDate: {type: Date, "default": Date.now}
});

var usersSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    name: {type: String, required: true},
    surname: {type: String, required: true},
    orders: [ordersSchema],
    rents: [rentsSchema],
    returns: [returnsSchema]
});

mongoose.model('user', usersSchema, 'users');
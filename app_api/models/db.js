var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/web_project_db';
if(process.env.NODE_ENV==='production'){
    dbURI=process.env.MLAB_URI;
}

mongoose.connect(dbURI, { useMongoClient: true });

mongoose.connection.on('connected', function() {
  console.log('Mongoose is connected on ' + dbURI);
});
mongoose.connection.on('error', function(err) {
  console.log('Mongoose error on connection: ' + err);
});
mongoose.connection.on('disconnected', function() {
  console.log('Mongoose is disconnected');
});

var properClosing = function(message, response) {
  mongoose.connection.close(function() {
    console.log('Mongoose stopped with message ' + message);
    response();
  });
};

// When restarting nodemon
process.once('SIGUSR2', function() {
  properClosing('nodemon restart', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// When exiting the application
process.on('SIGINT', function() {
  properClosing('exiting the application', function() {
    process.exit(0);
  });
});

// When exiting the application on Heroku
process.on('SIGTERM', function() {
  properClosing('exiting the application on Heroku', function() {
    process.exit(0);
  });
});

require("./users");
require("./books");
require("./quotes");
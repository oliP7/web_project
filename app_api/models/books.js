var mongoose = require('mongoose');

var booksSchema = new mongoose.Schema({
    title: {type: String, required: true},
    author: {type: String, required: true},
    ISBN: {type:String},
    link: {type:String},
    subject: {type:String},
    numberOfCopies: {type:Number},
    releaseDate: {type:Date},
    image: {type:String}
});

mongoose.model('book', booksSchema, 'books');
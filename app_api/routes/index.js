var express = require('express');
var router = express.Router();
var ctrlUsers = require('../controllers/users');
var ctrlDbPage = require('../controllers/db');
var ctrlBooks = require('../controllers/books');

router.post('/user/:username/:password', ctrlUsers.userExists);
router.post('/createUser/:username/:password/:name/:surname', ctrlUsers.saveUser);
router.get('/deleteDb', ctrlDbPage.deleteAllData);
router.get('/initDb', ctrlDbPage.initDbData);
router.get('/user/:username', ctrlUsers.findUser);
router.get('/users', ctrlUsers.listUsers);

router.post('/createBook/:title/:author/:ISBN/:link/:subject/:numberOfCopies/:releaseDate/:image', ctrlBooks.saveBook);


module.exports = router;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var uglifyJs = require('uglify-js');
var fs = require('fs');

require('./app_api/models/db');

var index = require('./app_server/routes/index');
var users = require('./app_server/routes/users');
var aboutUs = require('./app_server/routes/aboutUs');
var addAbook = require('./app_server/routes/addAbook');
var booking = require('./app_server/routes/booking');
var books = require('./app_server/routes/books');
var contact = require('./app_server/routes/contact');
var createAccount = require('./app_server/routes/createAccount');
var editBook = require('./app_server/routes/editBook');
var editProfile = require('./app_server/routes/editProfile');
var forgetPassword = require('./app_server/routes/forgetPassword');
var indexAdmin = require('./app_server/routes/indexAdmin');
var login = require('./app_server/routes/login');
var managingBooks = require('./app_server/routes/managingBooks');
var managingOrders = require('./app_server/routes/managingOrders');
var managingUsers = require('./app_server/routes/managingUsers');
var myProfile = require('./app_server/routes/myProfile');
var statistics = require('./app_server/routes/statistics');
var userDetails = require('./app_server/routes/userDetails');
var dbPage = require('./app_server/routes/db');
var indexApi = require('./app_api/routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use('/', index);
app.use('/index', index);
app.use('/users', users);
app.use('/aboutUs', aboutUs);
app.use('/addAbook', addAbook);
app.use('/booking', booking);
app.use('/books', books);
app.use('/contact', contact);
app.use('/createAccount', createAccount);
app.use('/editBook', editBook);
app.use('/editProfile', editProfile);
app.use('/forgetPassword', forgetPassword);
app.use('/indexAdmin', indexAdmin);
app.use('/login', login);
app.use('/managingBooks', managingBooks);
app.use('/managingOrders', managingOrders);
app.use('/managingUsers', managingUsers);
app.use('/myProfile', myProfile);
app.use('/statistics', statistics);
app.use('/userDetails', userDetails);
app.use('/db',dbPage);
app.use('/api', indexApi);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

module.exports = app;

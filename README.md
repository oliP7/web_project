# Online library

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

* Link to the web application on Heroku you can find here [application](https://onlinelibrary2017.herokuapp.com/index).

### Installation:

Since the application is in Node js we firs need node but in Cloud 9 environment the Node.js and npm are already installed.
As we already know each Node.js application may have dependencies defined in the **package.json** so we need to run the following command:

```
npm install
```

Next what we need is the Express generator and we install it by using the following command:

```
npm install -g express-generator
```

Next we need to install MongoDB in Cloud 9 and we do that with these commands:

```
sudo apt-get remove mongodb-org mongodb-org-server
sudo apt-get autoremove
sudo rm -rf /usr/bin/mongo*
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install mongodb-org mongodb-org-server
sudo touch /etc/init.d/mongod
sudo apt-get install mongodb-org-server
```

Another thing we need is the mongoose driver:

```
npm install --save mongoose
```

The last thing we need is to install nodemon with:

```
npm install -g nodemon
```

There are two ways of starting the application :

* The first way is with a locally with the **nodemon** command. But in order this to work we should create a database which must be called **web_project_db** .

* Or the second way is to run it with the db which we have on mLab site and the command to run the application with this db is
 **NODE_ENV=production MLAB_URI=mongodb://dbUser:WebProg4@ds137826.mlab.com:37826/web_project_db nodemon**.

## Application description

For the project topic of this course we chose to make an Online Library where users can browse and order books online and they can pick them up later without having to wait for lines and queues.
The application will be available online and it will have the possibilty for users to login and sign up if necessary. Also there will be an admin part of the application which will enable the administrator of the
library to add, edit or delete books from the database. The application will be built using the technologies mastered at the course Web Programing. In this README file the wireframes of the project are available,
for desktop and mobile screens.

Desktop wireframes:

The homepage of the application will look as followed on the picture, with a main menu, buttons for login and sign up and book covers presented with the option for a search.

![This is a homepage](docs/desktop/Homepage.png)

If the user does not have an account in our online library and he or she wishes to use our services he needs to create an account on the page bellow and he can do that by clicking on the create account button on
the homepage. For an account the user must provide an e-mail, an username and a password.

![This is a page for creating an account](docs/desktop/Create_account.png)

If the user already has an existing account all they have to do is log in the application. They can do that by clicking on the Login button on the homepage, and a form will apear asking for an e-mail and a
password. If the user has forgotten the password there is a link "Forget password".

![This is a page for login](docs/desktop/Login.png)

In such a case where the user has forgotten his password the page bellow will ask him for his mail and a reset password will be sent to that mail.

![This is a page for login](docs/desktop/Forget_password.png)

If the logging in has been successful then the user is directed at the homepage, but this time on the top right corner he can see that he is logged in and by clicking on the profile picture he can see his
profile information. The rest of the homepage is the same as described in the begining of the wireframes.

![This is a homepage when user is logged in](docs/desktop/HomepageUser.png)

On the main menu there is a button "About Us" and by clicking on it the user sees the page below. On this page he can see what is our application about and get some basic information about it.

![This is a page "About us"](docs/desktop/About_us.png)

Next to the "About us" button on the menu there is a "Contact" button which leads to the page with information about how to contact us. The user can see our location, e-mail, phone number and what time are
we open.

![This is a page "Contact"](docs/desktop/Contact.png)

By clicking on the profile picture the user can see his profile in our online library as shown bellow. He can see what are the books he ordered, with an option to cancel those orders, what books he has
rented and when is the due date for their retreivel and books he has returned. Also on this page he can click on the "Edit profile" button if he wishes to change the information about him and by 
clicking on the "Sign out" button his session is over and he is returned to the initial homepage.

![This is a page "My Profile"](docs/desktop/My_profile.png)

When changing the information about him, the user sees the Edit Profile page and he can edit all of his information: name, surname, password, username and e-mail.

![This is a page "Edit Profile"](docs/desktop/Edit_profile.png)

On the homepage the user can search books by their name, author or topic. When he clicks the search button the application will list the search results in a readable list, where the user can see some
of the information of the books that match his search. If the user finds the book he was looking for he can click on its cover for more information and for the option to order it.

![This is a page "Search"](docs/desktop/Search.png)

When the user finds the book he wants, he can see more information about it as well as how many available copies are there in the library. If the number of available copies is greater then 0 then the user
can click on the button "Order a book" and the book will apear in the list of ordered books in his profile which we discussed earlier.

![This is a page "Search"](docs/desktop/Booking.png)

The administrator of the library also has to login in the application using the same form, but the homepage that apears to him is different than the one the users see. On this homepage the admin has 4
major options: Adding a book, Managing books, Managing users and Statistics.

![This is a homepage when administrator is logged in](docs/desktop/HomepageAdmin.png)

When the administrator has a new book to add to the Online library he gets a form to fill out with the information of the book such as: title, author, ISBN, link, subject, number of copies, date of 
release and a cover picture if possible. After filling the form the administrator clicks on the "Add a book" button and the new book is added in the database and available for the users of the online
library.

![This is a page for adding books for the admin](docs/desktop/Adding_a_book.png)

The administrator can also search and browse books and the page looks very similar to the searching option of the regular users, but the administrator has a button "Edit" under every book and he can click
it if he needs to change some of the information about the book in question.

![This is a page for managing books for the admin](docs/desktop/Managing_books.png)

When the administrator finds the book he wants to edit a similar form as for adding a book apears, where he can edit the same information and save the changes he made by clicking on the button
"Save changes".

![This is a page for editing books for the admin](docs/desktop/Editing_books.png)

The administrator has the option to handle information about regular users. In the page bellow the form is shown where he can search for a particular user and by clicking on the "Details" button he can
see all of the users information.

![This is a page for managing users for the admin](docs/desktop/Managing_users.png)


After clicking on the "Details" button on the previous page the administrator can se all the information of the user and if there is a reason he can change them. Bellow the users picture there is a 
button "Manage orders" which can show the administrator the order list of the particular user.

![This is a page for users details for the admin](docs/desktop/User_Details.png)

For every user the administrator can see the list of ordered books and the list of rented books and by every book there is the option to rent it if it is ordered and the user has come to pick it up, or 
to mark it as returned if the user is returning it.

![This is a page for managing orders for the admin](docs/desktop/Manage_User_Orders.png)

Finally the administrator has the option to see the statistics of our application, such as which are the most active users or which users currently have rented the most books.

![This is a page for statistics for the admin](docs/desktop/Statistics.png)

In the next part the wireframes for mobile screens will be presented, they have the same functionalities as the desktop screens so we will not describe them individually:

Initial homepage of the application.

![This is a homepage](docs/mobile/Homepage.png)

Creating an account on a mobile screen.

![This is a page for creating an account](docs/mobile/Create_account.png)

Logging in from a mobile screen.

![This is a page for login](docs/mobile/Login.png)

The mobile screen for a forgotten password.

![This is a page for login](docs/mobile/Forget_password.png)

The homepage of a regular user after logging in on a mobile screen.

![This is a homepage when user is logged in](docs/mobile/HomepageUser.png)

The "About us" page on a mobile screen.

![This is a page "About us"](docs/mobile/About_us.png)

The "Contact" page on a mobile screen.

![This is a page "Contact"](docs/mobile/Contact.png)

The profile page of the user on a mobile screen.

![This is a page "My Profile"](docs/mobile/My_profile.png)

The form for changing his information the user sees on a mobile screen.

![This is a page "Edit Profile"](docs/mobile/Edit_profile.png)

Search results of books for a regular user on a mobile screen.

![This is a page "Search"](docs/mobile/Search.png)

Ordering a book on a mobile screen.

![This is a page "Search"](docs/mobile/Booking.png)

The homepage of the administrator when he logs in on a mobile screen.

![This is a homepage when administrator is logged in](docs/mobile/HomepageAdmin.png)

Adding a new book on a mobile screen.

![This is a page for adding books for the admin](docs/mobile/Adding_a_book.png)

Managing books on a mobile screen.

![This is a page for managing books for the admin](docs/mobile/Managing_books.png)

Editing the information of a book on a mobile screen.

![This is a page for editing books for the admin](docs/mobile/Editing_books.png)

Managing users on a mobile screen.

![This is a page for managing users for the admin](docs/mobile/Managing_users.png)

Editing user information on a mobile screen.

![This is a page for users details for the admin](docs/mobile/User_Details.png)

Managing the orders of a particular user on a mobile screen.

![This is a page for managing orders for the admin](docs/mobile/Manage_User_Orders.png)

Statistics information on a mobile screen.

![This is a page for statistics for the admin](docs/mobile/Statistics.png)
var request = require('request');
const errorLog = require('../controllers/logger').errorlog;
const successlog = require('../controllers/logger').successlog;
var apiURL = {
    server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}
/* GET createAccount page. */
module.exports.login = function (req, res, next) {
    res.render('login');
}

module.exports.checkLogin = function (req, res, next) {
    var data = req.body;
    var username = data.email;
    var password = data.password;

    if (username==="admin@admin" && password==="admin123"){
        res.render('indexAdmin');
    }else {
        var apiParams = {
            url: apiURL.server + "/api/user/" + username + "/" + password,
            method: 'POST',
            json: {}
        };

        request(
            apiParams,
            function(err, response, content) {
                if (content.exists) {
                    res.cookie("session","on",{expires:new Date(Date.now() + (3*24*60*60*1000))});
                    res.cookie("user",username,{expires:new Date(Date.now() + (3*24*60*60*1000))});
                    successlog.info(`Success Message for User: ${username}`);
                    res.render('index');
                } else {
                    res.render('login', {error: true, username: username, password: password});
                }
            }
        );
    }

}
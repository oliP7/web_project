/* GET home page. */
const successlog = require('../controllers/logger').successlog;

module.exports.index = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page index`);
  res.render('index');
}
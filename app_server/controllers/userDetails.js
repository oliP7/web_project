/* GET userDetails page. */
const successlog = require('../controllers/logger').successlog;

module.exports.userDetails = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page userDetails`);
  res.render('userDetails');
}
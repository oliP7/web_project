/* GET managingUsers page. */
const successlog = require('../controllers/logger').successlog;

module.exports.managingUsers = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ' clicked on page managingUsers');
  res.render('managingUsers');
}
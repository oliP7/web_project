/* GET contact page. */
const successlog = require('../controllers/logger').successlog;

module.exports.contact = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page contact`);
  res.render('contact');
}
/* GET aboutUs page. */
const successlog = require('../controllers/logger').successlog;

module.exports.aboutUs = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page aboutUs`);
  res.render('aboutUs');
}
const winstonRotator = require('winston-daily-rotate-file');
var winston = require('winston');

const consoleConfig = [
  new winston.transports.Console({
    'colorize': true
  })
];

const fileConfig = [
    new (winston.transports.File)({ filename: './logs/logs.log' })
];

const createLogger = new winston.Logger({
  'transports': fileConfig
});

const successLogger = createLogger;
successLogger.add(winstonRotator, {
  'name': 'access-file',
  'level': 'info',
  'filename': './logs/access.log',
  'json': false,
  'datePattern': 'yyyy-MM-dd-',
  'prepend': true
});

const errorLogger = createLogger;
errorLogger.add(winstonRotator, {
  'name': 'error-file',
  'level': 'error',
  'filename': './logs/error.log',
  'json': false,
  'datePattern': 'yyyy-MM-dd-',
  'prepend': true
});

module.exports = {
  'successlog': successLogger,
  'errorlog': errorLogger
};
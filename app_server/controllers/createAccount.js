var request = require('request');
const successlog = require('../controllers/logger').successlog;

var apiURL = {
    server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}
/* GET createAccount page. */
module.exports.createAccount = function(req, res) {
  res.render('createAccount');
}

module.exports.createUser = function (req, res, next) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page createAccount`);
  var data = req.body;
  var username = data.email;
  var password = data.pwd1;
  var name = data.name;
  var surname = data.surname;

  var apiParams = {
      url: apiURL.server + "/api/createUser/" + username + "/" + password + "/" + name + "/" + surname,
      method: 'POST',
      json: {}
  };
    request(
        apiParams,
        function(err, response, content) {
            if (content.saved) {
                res.render('login');
            } else {
                res.render('createAccount', {error: true, username: username, password: password});
            }
        }
    );
}
/* GET managingOrders page. */
const successlog = require('../controllers/logger').successlog;

module.exports.managingOrders = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page managingOrders`);
  res.render('managingOrders');
}
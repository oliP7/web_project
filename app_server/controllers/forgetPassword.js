/* GET forgetPassword page. */
const successlog = require('../controllers/logger').successlog;

module.exports.forgetPassword = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page forgetPassword`);
  res.render('forgetPassword');
}
var request = require('request');
const successlog = require('../controllers/logger').successlog;

var apiURL = {
    server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}

/* GET addAbook page. */
module.exports.addAbook = function(req, res) {
  res.render('addAbook');
}

module.exports.createBook = function (req, res, next) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page addAbook`);
  var data = req.body;
  console.log(data);
  var bookTitle = data.book_title;
  var bookAuthor = data.book_author;
  var bookISBN = data.book_ISBN;
  var bookLink = data.book_link;
  var bookSubject = data.book_subject_choosen;
  var bookNumOfCopies = data.book_quantity;
  var bookReleaseDate = data.book_releaseDate;
  var bookImg = data.book_image;
  
  var bookDate = bookReleaseDate.replace("/","-");
  
  console.log(bookSubject +' bookkkkk ' + bookImg);

  var apiParams = {
      url: apiURL.server + "/api/createBook/" + bookTitle + "/" + bookAuthor + "/" + bookISBN + "/" + bookLink + "/" + bookSubject + "/" + bookNumOfCopies + "/" + bookDate + "/" + bookImg,
      method: 'POST',
      json: {}
  };
    request(
        apiParams,
        function(err, response, content) {
            if (content.saved) {
                res.render('indexAdmin');
            } else {
                res.render('addAbook', {error: true, title: content.bookTitle, author: content.bookAuthor, ISBN: content.bookISBN});
            }
        }
    );
}
/* GET editBook page. */
const successlog = require('../controllers/logger').successlog;

module.exports.editBook = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page editBook`);
  res.render('editBook');
}
/* GET booking page. */
const successlog = require('../controllers/logger').successlog;

module.exports.booking = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page booking`);
  res.render('booking');
}
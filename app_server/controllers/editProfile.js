var request = require('request');
const successlog = require('../controllers/logger').successlog;

var apiURL = {
    server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}
/* GET editProfile page. */
module.exports.editProfile = function(req, res) {
    var user = req.cookies.user;
    successlog.info(user + ` clicked on page editProfile`);
    var userInfo = req.params.username;
    var apiParams = {
        url: apiURL.server + "/api/user/" + userInfo,
        method: 'GET',
        json: {}
    };
    request(
        apiParams,
        function(err, response, content) {
            if (content.exists) {
                res.render('editProfile', {username: content.username, password: "", password2 : "", name: content.name, surname: content.surname});
            } else {
                res.render('editProfile', {error: true, username: content.username, password: "", password2 : "", name: content.name, surname: content.surname});
            }
        }
    );
}

module.exports.editUserInfo = function (req, res) {

}
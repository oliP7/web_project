/* GET managingBooks page. */
const successlog = require('../controllers/logger').successlog;

module.exports.managingBooks = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page managingBooks`);
  res.render('managingBooks');
}
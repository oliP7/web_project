/* GET myProfile page. */
const successlog = require('../controllers/logger').successlog;

module.exports.myProfile = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page myProfile`);
  res.render('myProfile');
}
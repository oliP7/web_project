var request = require('request');
const successlog = require('../controllers/logger').successlog;

var apiURL = {
    server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}

/* GET db page. */
module.exports.db = function(req, res) {
    res.render('db');
}
module.exports.deleteDb = function(req, res) {
    var user = req.cookies.user;
    successlog.info(user + ` clicked on page db`);
    var apiParams = {
        url: apiURL.server + "/api/deleteDb",
        method: 'GET',
        json: {}
    };
    request(
        apiParams,
        function(err, response, content) {
            if (content.deleted) {
                res.render('db', {success: true});
            } else {
                res.render('db', {error: true});
            }
        }
    );
}

module.exports.initValues = function (req, res) {
    var apiParams = {
        url: apiURL.server + "/api/initDb",
        method: 'GET',
        json: {}
    };
    request(
        apiParams,
        function(err, response, content) {
            console.log(content.inserted+" da li je dosao ovde?");
            if (content.inserted) {
                res.render('db', {successInsert: true});
            } else {
                res.render('db', {errorInsert: true});
            }
        }
    );
}
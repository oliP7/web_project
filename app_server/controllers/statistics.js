/* GET statistics page. */
const successlog = require('../controllers/logger').successlog;

module.exports.statistics = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page statistics`);
  res.render('statistics');
}
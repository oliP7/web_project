/* GET indexAdmin page. */
const successlog = require('../controllers/logger').successlog;

module.exports.indexAdmin = function(req, res) {
  var user = req.cookies.user;
  successlog.info(user + ` clicked on page indexAdmin`);
  res.render('indexAdmin');
}
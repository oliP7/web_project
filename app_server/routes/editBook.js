var express = require('express');
var router = express.Router();
var ctrlEditBook = require('../controllers/editBook');

/* GET editBook page */
router.get('/', ctrlEditBook.editBook);

module.exports = router;

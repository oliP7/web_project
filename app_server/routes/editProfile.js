var express = require('express');
var router = express.Router();
var ctrlEditProfile = require('../controllers/editProfile');

/* GET editProfile page */
router.get('/:username', ctrlEditProfile.editProfile);
router.post('/', ctrlEditProfile.editUserInfo);

module.exports = router;

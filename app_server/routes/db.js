var express = require('express');
var router = express.Router();
var ctrlDb = require('../controllers/db');

/* GET db page */
router.get('/', ctrlDb.db);
router.get('/deleteDb', ctrlDb.deleteDb);
router.get('/initValues', ctrlDb.initValues);

module.exports = router;
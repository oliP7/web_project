var express = require('express');
var router = express.Router();
var ctrlAddABook = require('../controllers/addAbook');

/* GET addAbook page */
router.get('/', ctrlAddABook.addAbook);
router.post('/', ctrlAddABook.createBook);

module.exports = router;

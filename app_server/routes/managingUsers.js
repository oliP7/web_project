var express = require('express');
var router = express.Router();
var ctrlManagingUsers = require('../controllers/managingUsers');

/* GET managingUsers page */
router.get('/', ctrlManagingUsers.managingUsers);

module.exports = router;

var express = require('express');
var router = express.Router();
var ctrlStatistics = require('../controllers/statistics');

/* GET statistics page */
router.get('/', ctrlStatistics.statistics);

module.exports = router;

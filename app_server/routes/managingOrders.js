var express = require('express');
var router = express.Router();
var ctrlManagingOrders = require('../controllers/managingOrders');

/* GET managingOrders page */
router.get('/', ctrlManagingOrders.managingOrders);

module.exports = router;

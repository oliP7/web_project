var express = require('express');
var router = express.Router();
var ctrlBooking = require('../controllers/booking');

/* GET booking page */
router.get('/', ctrlBooking.booking);

module.exports = router;

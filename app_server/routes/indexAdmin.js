var express = require('express');
var router = express.Router();
var ctrlIndexAdmin = require('../controllers/indexAdmin');

/* GET indexAdmin page */
router.get('/', ctrlIndexAdmin.indexAdmin);

module.exports = router;

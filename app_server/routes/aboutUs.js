var express = require('express');
var router = express.Router();
var ctrlAboutUs = require('../controllers/aboutUs');

/* GET aboutUs page */
router.get('/', ctrlAboutUs.aboutUs);

module.exports = router;

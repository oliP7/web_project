var express = require('express');
var router = express.Router();
var ctrlForgetPassword = require('../controllers/forgetPassword');

/* GET forgetPassword page */
router.get('/', ctrlForgetPassword.forgetPassword);

module.exports = router;

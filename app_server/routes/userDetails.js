var express = require('express');
var router = express.Router();
var ctrlUserDetails = require('../controllers/userDetails');

/* GET userDetails page */
router.get('/', ctrlUserDetails.userDetails);

module.exports = router;

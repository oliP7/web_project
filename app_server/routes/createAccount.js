var express = require('express');
var router = express.Router();
var ctrlCreateAccount = require('../controllers/createAccount');

/* GET createAccount page */
router.get('/', ctrlCreateAccount.createAccount);
router.post('/', ctrlCreateAccount.createUser);

module.exports = router;

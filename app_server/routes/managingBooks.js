var express = require('express');
var router = express.Router();
var ctrlManagingBooks = require('../controllers/managingBooks');

/* GET managingBooks page */
router.get('/', ctrlManagingBooks.managingBooks);

module.exports = router;

var express = require('express');
var router = express.Router();
var ctrlMyProfile = require('../controllers/myProfile');

/* GET myProfile page */
router.get('/', ctrlMyProfile.myProfile);

module.exports = router;

/* global angular */
angular.module('onlinelibrary', []);

var usersData = function ($http) {
    return $http.get('/api/users');
};

var booksListCtrl = function($scope){
    $scope.data ={
        books: [
            {"title":"Anna Karenina","author":"Lav Tolstoj", "isbn":"25352", "link":"", "subject":"1","numberOfCopies":"10", "releaseDate": Date.now(),"image":"/images/anaKarenjina.jpg","_id": "5a0edde9490d6caf4cf68220"},
            {"title":"One Hundred Years of Solitude","author":"Gabriel Garcia Marquez", "isbn":"25352", "link":"", "subject":"1","numberOfCopies":"10", "releaseDate": Date.now(),"image":"/images/stoGodinaSamoce.jpg","_id": "5a0edde9490d6caf4cf68221"},
            {"title":"Death and the Dervish","author":"Mesa Selimovic", "isbn":"25352", "link":"", "subject":"1","numberOfCopies":"10", "releaseDate": Date.now(),"image":"/images/dervisISmrt.jpg","_id": "5a0edde9490d6caf4cf68222"},
            {"title":"Antigona","author":"Sofocles", "isbn":"25352", "link":"", "subject":"1","numberOfCopies":"10", "releaseDate": Date.now(),"image":"/images/antigona.jpg","_id": "5a0edde9490d6caf4cf68223"},
            {"title":"Shadows of the Pomegranate Tree","author":"Tariq Ali", "isbn":"25352", "link":"", "subject":"1","numberOfCopies":"10", "releaseDate": Date.now(),"image":"/images/sjene.jpg","_id": "5a0edde9490d6caf4cf68224"}
        ]
    };
};
var usersListCtrl = function($scope, usersData){
    usersData.then(
        function success(response) {
            $scope.data = {users: response.data};

        });

    $scope.filter = "username";
    $scope.search = {name:'', surname:'', username:''};
    $scope.changeFilterTo = function(pr) {
        $scope.filter = pr;
    }
}; 

var showBookDetails = function(book){
    var bookTag = "\
        .col-sm-2#'"+book.id+"'\
            a(style="+clickable()+",href='booking')\
                img.img-responsive(src='"+book.image+"', height='200px')\
            input#allBooksShowBtn_"+book.id+"'(type='button', value='Book details')\
            span.popuptext#masterDetailPopUp\
    ";
    return bookTag;
};

 
angular
    .module('onlinelibrary')
    .controller('usersListCtrl', usersListCtrl)
    .controller('booksListCtrl', booksListCtrl)
    .directive('showBookDetails', showBookDetails)
    .service('usersData', usersData);

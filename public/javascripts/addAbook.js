jQuery(document).ready(function () {
    var books = JSON.parse(localStorage.getItem("allBooks"));
    console.log(books);
    $("#btn-addABook").click(function (event) {
        var id = "id20";
        var image = "/images/newBook.jpeg";
        var author = $("#book_author");
        var name = $("#book_title");
        var availableQuantity = $("#book_quantity");
        books.data.push({"id":id,"name":name,"author":author, "image":image,"availableQuantity":10});
        localStorage.setItem("allBooks",JSON.stringify(books));
    });
});
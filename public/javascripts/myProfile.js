jQuery(document).ready(function () {
    var orders = JSON.parse(localStorage.getItem("orderedBooks"));
    console.log(orders);
    jQuery.each(orders.data, function (oid, orderedBook) {
        console.log(orderedBook);
        $("#orderedBooksCol").append("\
        <div id='orderedBooks-"+orderedBook.id+"'>\
            <img class='img-responsive' src='"+orderedBook.image+"'  height='200px'></img>\
            <h5>"+orderedBook.name+"</h5>\
            <h6>Author:"+orderedBook.author+"</h6>\
            <button type='button' class='btn btn-danger'>Delete</button>\
            </div>");
    });
    console.log(orders.data.length);

    $(".btn.btn-danger").click(function (event) {
        console.log($(".btn.btn-danger").parent());
        console.log($(".btn.btn-danger").parent().prop("id"));
        var divId =$(".btn.btn-danger").parent().prop("id");
        var idOrder = divId.split('-');
        console.log(idOrder[0]);
        console.log(idOrder[1]);
        var index;
        jQuery.each(orders.data, function (oid, orderedBook) {
           if(orderedBook.id==idOrder[1]){
               console.log(orderedBook.id);
               console.log(orders.data[oid]);
               index = orders.data.indexOf(orderedBook);
               console.log(index);
           }
        });
        orders.data.splice(index,1);
        console.log(JSON.stringify(orders.data));
        console.log(orders.data);
        localStorage.setItem("orderedBooks",JSON.stringify(orders));
        location.reload();

    });

    $("#buttonEditProfile").click(function (event) {
        event.preventDefault();
        var userInfo = readCookie('user');
        // userInfo.replace('%40','@');
        window.location = "/editProfile/"+userInfo;
        console.log(userInfo);
    });
});

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}
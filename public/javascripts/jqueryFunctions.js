// jQuery.noConflict();
var popularBooksList = {
    "data":[
        {"id":"id1","name":"Madame Bovary","author":"Gustave Flaubert", "image":"/images/bovari.jpg","availableQuantity":10},
        {"id":"id2","name":"The Stranger","author":"Albert Camus", "image":"/images/stranac.jpg","availableQuantity":10},
        {"id":"id3","name":"Crime and Punishment","author":"Fjodor M. Dostojevski", "image":"/images/zlocinIKazna.jpg","availableQuantity":10},
        {"id":"id4","name":"Iliad and Odyssey","author":"Homer", "image":"/images/ilijada.jpg","availableQuantity":10},
        {"id":"id5","name":"Withering Heights","author":"Emily Bronte", "image":"/images/orkanskivisovi.jpg","availableQuantity":10}

    ]
};
var allBooksList = {
    "data":[
        {"id":"id6","name":"Anna Karenina","author":"Lav Tolstoj", "image":"/images/anaKarenjina.jpg","availableQuantity":10},
        {"id":"id7","name":"One Hundred Years of Solitude","author":"Gabriel Garcia Marquez", "image":"/images/stoGodinaSamoce.jpg","availableQuantity":10},
        {"id":"id8","name":"Death and the Dervish","author":"Meša Selimović", "image":"/images/dervisISmrt.jpg","availableQuantity":10},
        {"id":"id9","name":"Antigona","author":"Sofocles", "image":"/images/antigona.jpg","availableQuantity":10},
        {"id":"id10","name":"Shadows of the Pomegranate Tree","author":"Tariq Ali", "image":"/images/sjene.jpg","availableQuantity":10}

    ]
};
var usersList = {"data":[
    {"username":"bozen@jovanovski.com","password":"pass123"},
    {"username":"amra@omanovic.com","password":"pass123"}
]};
var orderedBooksList = {
    "data":[
        {"id":"id1","name":"Withering Heights","author":"Emily Bronte", "image":"/images/orkanskivisovi.jpg","availableQuantity":10},
        {"id":"id2","name":"The Stranger","author":"Albert Camus", "image":"/images/stranac.jpg","availableQuantity":10}

    ]
};

jQuery(document).ready(function(){

	var sessionCookie = readCookie('session');

    // if(localStorage.getItem("users")===null) {
    //     saveUsersToLocalStorage();
    // }
	if(localStorage.getItem("popularBooks")===null){
	    localStorage.setItem("popularBooks",JSON.stringify(popularBooksList));
    }
    if(localStorage.getItem("allBooks")===null){
	    localStorage.setItem("allBooks",JSON.stringify(allBooksList));
    }
    if(localStorage.getItem("orderedBooks")===null){
	    localStorage.setItem("orderedBooks",JSON.stringify(orderedBooksList));
    }

	if(sessionCookie!=null && sessionCookie=='on'){
		$("#imgContainerId").show();
		$("#logutBtn").show();
		$("#createAccountBtn").hide();
		$("#loginBtn").hide();
	}else{
        $("#imgContainerId").hide();
        $("#logutBtn").hide();
        $("#createAccountBtn").show();
        $("#loginBtn").show();
	}

	// eraseCookie('session');

    // $("#errorMessage").hide();
	
	$(".dropdown-menu li a").click(function(){
	$(this).parents(".btn-group").find('.selection').text($(this).text());
	$(this).parents(".btn-group").find('.selection').val($(this).text());
	});

    $("#logutBtn").click(function (event) {
        event.preventDefault();
		eraseCookie('session');
		eraseCookie('user');
        window.location = "index";
    });
    
    $(function () {
        $('#datetimepicker1').datetimepicker({
            viewMode: 'years',
            format: 'MM/YYYY'
        });
    });
    $('#book_subject_choosen').hide();
    $('#subjectMenu li').on('click', function(){
        $('#book_subject_choosen').val($('#book_subject').text());
    });
    $('#book_subject_choosen').val($('#book_subject').text());
    
   
});

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function saveUsersToLocalStorage() {
    localStorage.setItem("users",JSON.stringify(usersList));
}


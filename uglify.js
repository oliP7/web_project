var NodeUglifier = require("node-uglifier");
var nodeUglifier = new NodeUglifier("app.js");
nodeUglifier.merge().uglify();

//exporting
nodeUglifier.exportToFile("lib_compiled/test/resultFiles/simpleMergeAndUglify.js");
nodeUglifier.exportSourceMaps("lib_compiled/test/resultFiles/sourcemaps/simpleMergeAndUglify.js");
//DONE

//in case you need it get the string
//if you call it before uglify(), after merge() you get the not yet uglified merged source
var uglifiedString=nodeUglifier.toString();